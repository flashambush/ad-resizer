
## Install

```bash
  npm i
```

## Build Template

```bash
  gulp compileTs copy compress
```

---

## How to Use

1. Определите значения ширин брейкпоинтов в `resize.styl` файле
2. Write and compile `style.styl` file

### CSS

#### Container styles
Класс добавляется к элементу `container`

* `x90` - Высота 90px
* `x120` - Высота 120px
* `x240` - Высота 240px
* `x250` - Высота 250px
* `x300` - Высота 300px

##### Пример

```html
<div class="container x240">
...
</div>
```

#### Layout styles

* `scale` - Масштабироваться пропорционально изменению размера креатива
* `xs` - отображается только в диапазоне от **0** до **sm**
* `sm` - отображается только в диапазоне от **sm** до **md**
* `md` - отображается только в диапазоне от **md** до **lg**
* `lg` - отображается только в диапазоне от **lg** до **xl**
* `xl` - отображается только в диапазоне от **xl** до **xxl**
* `xxl` - отображается только в диапазоне от **xll**

##### Пример

```html
<div class="container x250">
    <div class="sm">sm</div>
    <div class="md">md</div>
    <div class="lg">lg</div>
    <div class="xl">xl</div>
    <div class="xxl">xxl</div>
</div>
```

### JS

##### scale(options)
* `element` - HTMLElement, который необходимо масштабировать
* `k` - коэфицент масштабирования
* `type` - тип масштабирования
    * `scale` - пропорциональное масштабирование в процентах
        * `minScale` - минимальное значение scale
        * `maxScale` - максимальное значение scale
    * `width` - пропорциональное масштабирование по ширине
        * `minWidth` - минимальная ширина
        * `maxWidth` - максимальная ширина
    * `height` - пропорциональное масштабирование по высоте
        * `minHeight` - минимальная высота
        * `maxHeight` - максимальная высота

##### Пример

```javascript
    window.addEventListener("load", function() {
        var fla = new Resizer(document.querySelector(".creative"), 970)
        fla.scale({element:document.querySelector(".logo"), k:1, type:"scale"})
        fla.scale({element:document.querySelector("img"), k:1, type:"width", minWidth:100, maxWidth: 300})
    })
```


---
