const gulp = require('gulp');
const fs = require('fs-extra');
const path = require('path');
const {spawn} = require('child_process');
const uglify = require('gulp-uglify');
const pipeline = require('readable-stream').pipeline;
const rename = require("gulp-rename");


const pack = fs.readJSONSync(path.join(__dirname, 'package.json'));


gulp.task('compress', function () {
    return pipeline(
        gulp.src('src/*.js'),
        //uglify(),
        rename({basename: 'resizer', suffix: '.min'}),
        gulp.dest('template')
    );
});

gulp.task('copy', () =>
    Promise.all([
        ...Object.keys(pack.dependencies).map(name =>
            fs.copy(path.join(__dirname, 'node_modules', name), path.join(__dirname, 'template', name))),
        fs.copy(path.join(__dirname, 'src', 'helper.styl'), path.join(__dirname, 'template', 'helper.styl')),
        fs.copy(path.join(__dirname, 'src', 'style.styl'), path.join(__dirname, 'template', 'style.styl')),
        fs.copy(path.join(__dirname, 'src', 'resize.styl'), path.join(__dirname, 'template', 'resize.styl')),
        fs.readFile(path.join(__dirname, 'src', 'template.html'))
            .then(template => fs.outputFile(path.join(__dirname, 'template', 'index.html'), template))
    ]));

gulp.task('compileTs', () => new Promise((resolve, reject) => {
    const tsc = spawn(path.join(__dirname, 'node_modules', '.bin', 'tsc'), ['-p', path.join(__dirname)]);

    tsc.stdout.on('data', (data) => {
        console.log(`stdout: ${data}`);
    });

    tsc.stderr.on('data', (data) => {
        console.log(`stderr: ${data}`);
    });

    tsc.on('close', code => {
        code ? reject() : resolve();
    });
}));


//gulp compileTs copy compress