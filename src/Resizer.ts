/**
 * Created by tsigelmac on 14/02/2019.
 */
class Resizer {
    private readonly defaultWidth: number;
    private readonly creativeDOM: HTMLElement;
    private readonly scaling: Array<TItems>;

    constructor(creative: HTMLElement, defaultWidth: number = 970) {
        this.defaultWidth = defaultWidth;
        this.creativeDOM = creative;
        this.scaling = Array.prototype.map.call(document.querySelectorAll('.scale') || [], (item: HTMLElement) =>
            ({ element: item, minScale: -Infinity, maxScale: Infinity, k: 1, type: 'scale' })) as Array<TItems>;

        const handler = (event: Event) => this.onResize(event);
        window.addEventListener('resize', handler);
    }

    public scale(item: Partial<TItems> & { type: TTypes, element: HTMLElement }): void {
        this.scaling.push(Resizer.fillItem(item));
    }

    private onResize(evt: Event): void {
        this.scaling.forEach(item => {
            let scale: number = this.creativeDOM.clientWidth / this.defaultWidth * item.k;
            switch (item.type) {
                case 'width':
                    scale = Math.min(Math.max(scale * item.dw, item.minWidth), item.maxWidth);
                    item.element.style.width = scale + "px";
                    break;
                case 'height':
                    scale = Math.min(Math.max(scale * item.dh, item.minHeight), item.maxHeight);
                    item.element.style.height = scale + "px";
                    break;
                case 'scale':
                    scale = Math.min(Math.max(scale, item.minScale), item.maxScale);
                    item.element.style.transform = 'scale(' + scale + ')';
                    break;
                default:
            }
        });
    }

    private static fillItem(data: Partial<TItems> & { type: TTypes, element: HTMLElement }): TItems {
        const item = { ...data };

        switch (item.type) {
            case 'width':
                item.maxWidth = item.maxWidth || Infinity;
                item.minWidth = item.minWidth || 0;
                item.dw = item.element.clientWidth;
                break;
            case 'height':
                item.maxHeight = item.maxHeight || Infinity;
                item.minHeight = item.minHeight || 0;
                item.dh = item.element.clientHeight;
                break;
            case 'scale':
                item.maxScale = item.maxScale || Infinity;
                item.minScale = item.minScale || -Infinity;
                break;
            default:
                throw new Error(`Wrong item type! ${data.type}`);
        }

        item.k = item.k || 1;

        return item as TItems;
    }
}

interface IBaseScaleItem {
    element: HTMLElement;
    k: number;
    type: string;
}

interface IScaleItem extends IBaseScaleItem {
    type: 'scale';
    minScale: number;
    maxScale: number;
}

interface IScaleItemByWidth extends IBaseScaleItem {
    type: 'width';
    minWidth: number;
    maxWidth: number;
    dw:number;
}

interface IScaleItemByHeight extends IBaseScaleItem {
    type: 'height';
    minHeight: number;
    maxHeight: number;
    dh:number;
}

type TTypes = 'scale' | 'width' | 'height';
type TItems = IScaleItem | IScaleItemByWidth | IScaleItemByHeight